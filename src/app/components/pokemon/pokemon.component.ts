import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {PokemonsService} from '../../services/pokemons.service';

@Component({
  selector: 'app-pokemon',
  templateUrl: './pokemon.component.html',
  styleUrls: ['./pokemon.component.css']
})
export class PokemonComponent implements OnInit {

  @Input() pokemons: any[] = [];

  constructor(private router: Router,
              private pokemonsService: PokemonsService) { }

  ngOnInit(): void {
    this.pokemonsService.getPokemons().subscribe((pokemons) => this.pokemons = pokemons);
  }

  public goToPokemon(pokemonId: string): void {
    this.router.navigate(['pokemon', pokemonId]);
  }
}
