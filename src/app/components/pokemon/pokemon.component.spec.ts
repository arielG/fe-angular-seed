import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PokemonComponent } from './pokemon.component';
import {of} from 'rxjs';
import {PokemonsService} from '../../services/pokemons.service';
import {By} from '@angular/platform-browser';
import {Router} from '@angular/router';

class FakeRouter {
  // tslint:disable-next-line:typedef
  navigate(params) {}
}

describe('PokemonComponent', () => {
  let component: PokemonComponent;
  let fixture: ComponentFixture<PokemonComponent>;
  let pokemonsService: any;

  beforeEach(async () => {
    const pokemonServiceSpy = jasmine.createSpyObj('PokemonsService', ['getPokemons']);

    await TestBed.configureTestingModule({
      declarations: [PokemonComponent]
      ,
      providers: [
        { provide: PokemonsService, useValue: pokemonServiceSpy },
        { provide: Router, useClass: FakeRouter }
      ]
    })
    .compileComponents()
    .then(() => {
      fixture = TestBed.createComponent(PokemonComponent);
      component = fixture.componentInstance;
      pokemonsService = TestBed.inject(PokemonsService);
    });
  });

  beforeEach(() => {
    const pokemons = [
      {
        "name": "bulbasaur",
        "ability": {
          "name": "overgrow",
          "description": null,
          "damage": null
        },
        "type": {
          "name": "grass",
          "move_damage_class": null
        }
      },
      {
        "name": "ivysaur",
        "ability": {
          "name": "overgrow",
          "description": null,
          "damage": null
        },
        "type": {
          "name": "grass",
          "move_damage_class": null
        }
      }
    ];
    pokemonsService.getPokemons.and.returnValue(of(pokemons));
    fixture.detectChanges();
  });

  it('should show pokemon list', () => {
    const pokemonList = fixture.debugElement.queryAll(By.css('.card'));
    expect(pokemonList.length).toBe(2);
  });

  it('should show pokemon name', () => {
    const pokemonNameList = fixture.debugElement.queryAll(By.css('.card-title'));
    const firstPokemonName = pokemonNameList[0].nativeElement.innerText;

    expect(firstPokemonName).toEqual('bulbasaur');
  });

  it('should redirect a pokemon/:id', () => {
    const router = TestBed.inject(Router);
    const routerSpy = spyOn(router, 'navigate');
    const id = '123';
    component.goToPokemon(id);

    expect(routerSpy).toHaveBeenCalledWith(['pokemon', id]);
  });
});
