import { Component, OnInit } from '@angular/core';
import {PokemonsService} from '../../services/pokemons.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  pokemons: any[];
  loading: boolean;

  constructor(private pokemonsService: PokemonsService) {
    this.loading = true;
    this.pokemons = pokemonsService.getPokemons()
      .subscribe((data: any) => {
        this.loading = false;
        this.pokemons = data;
        console.log(data);
      });
  }

  ngOnInit(): void {
  }

}
