import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class PokemonsService {

  constructor( private httpClient: HttpClient) {
    console.log('PorkemonsService constructed');
  }

  getQuery(query: string): any {
    return this.httpClient.get(`http://localhost:8080/api/v1/${query}`);
  }

  getPokemons(): any {
    return this.getQuery('pokemon');
  }
}
